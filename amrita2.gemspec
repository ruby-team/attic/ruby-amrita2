require 'lib/amrita2/version'

Gem::Specification.new do |s|

  s.name = 'amrita2'
  s.version = Amrita2::Version::STRING
  s.platform = Gem::Platform::RUBY
  s.summary =
    "Amrita2 is a a xml/xhtml template library for Ruby"
  s.files = Dir.glob("{lib,specs,sample}/**/*") << "README" << "Rakefile" << "init.rb"
  s.require_path = 'lib'
  s.homepage = 'http://retro.brain-tokyo.net/projects/amrita2/wiki/Amrita2'
  s.rubyforge_project = 'amrita2'


  s.has_rdoc=true
  s.rdoc_options  << "--main" << "README"
  s.extra_rdoc_files = ["README"]

  s.add_dependency('hpricot', '>= 0.6.0')

  s.author = "Taku Nakajima"
  s.email = "tnakajima@brain-tokyo.jp"
end
