
require 'rexml/document'
require 'amrita2'
require 'amrita2/testsupport'

include Amrita2::Filters
include Amrita2::Runtime

context "Attribute" do
  
  specify "set attribute" do
    #t = Amrita2::Template.new('<div><a am:src="aaa|Attr[:href]">aaa</a></div>')
    t = Amrita2::Template.new <<-END
      <<div<
        <<a :aaa|Attr[:href]<
          xyz
    END
    t.test_with(:aaa => {
            :href=>"http://amrita2.rubyforge.org/",
                }
                ) do |r|
      r.should_be_samexml_as '<div><a href="http://amrita2.rubyforge.org/">xyz</a></div>'
    end
  end
  
  specify "delete attribute" do
    #t = Amrita2::Template.new('<div><a class="highlight" am:src="aaa|Attr[:href, :class, :body]">aaa</a></div>')
    t = Amrita2::Template.new <<-END
      <<div<
        <<a.highlight :aaa | Attr[:href, :class, :body] <
          aaa
    END
    t.test_with(:aaa => {
                  :href=>"http://amrita2.rubyforge.org/",
                  :class=>true,
                  :body=>"Amrita"
                }
                ) do |r|
      r.should_be_samexml_as '<div><a class="highlight" href="http://amrita2.rubyforge.org/">Amrita</a></div>'
    end
    t.test_with(:aaa => {
                  :href=>"http://amrita2.rubyforge.org/",
                  :class=>false,
                  :body=>"Amrita"
                }
                ) do |r|
      r.should_be_samexml_as '<div><a href="http://amrita2.rubyforge.org/">Amrita</a></div>'
    end
    t.test_with(:aaa => {
                  :href=>"http://amrita2.rubyforge.org/",
                  :class=>nil,
                  :body=>"Amrita"
                }
                ) do |r|
      r.should_be_samexml_as '<div><a href="http://amrita2.rubyforge.org/">Amrita</a></div>'
    end
  end
  
  specify "delete attribute2" do
    t = Amrita2::Template.new('<<div#cart : | Attr[:style] >>')
    t.test_with(:style => "display: none") do |r|
      r.should_be_samexml_as '<div id="cart" style="display: none" />'
    end
    t.test_with(:style => nil) do |r|
      r.should_be_samexml_as '<div id="cart" />'
    end
    t.test_with(:style => false) do |r|
      r.should_be_samexml_as '<div id="cart" />'
    end
    t.test_with(:style => "") do |r|
      r.should_be_samexml_as '<div id="cart" style=""/>'
    end
  end
  
  specify "change attribute key" do
    t = Amrita2::Template.new('<div><a am:src="aaa|Attr[:body=>true, :href=>:url]">aaa</a></div>')
    t.test_with(:aaa => {
                    :url=>"http://amrita2.rubyforge.org/",
                    :body=>"Amrita"
                  }
                  ) do |r|
      r.should_be_samexml_as '<div><a href="http://amrita2.rubyforge.org/">Amrita</a></div>'
    end
  end
  
  specify "key with space" do
    t = Amrita2::Template.new(%[<div><a am:src="aaa" am:filter="Attr[:body=>true, :href=>:'my url']">aaa</a></div>])
    t.test_with(:aaa => {
                    :"my url"=>"http://amrita2.rubyforge.org/",
                    :body=>"Amrita"
                  }
                  ) do |r|
      r.should_be_samexml_as '<div><a href="http://amrita2.rubyforge.org/">Amrita</a></div>'
    end
  end
  
  specify "change body key" do
    t = Amrita2::Template.new('<div><a am:src="aaa|Attr[:href=>:url, :body=>:text]">aaa</a></div>')
    t.test_with(:aaa => {
                    :url=>"http://amrita2.rubyforge.org/",
                    :text=>"Amrita"
                  }
                  ) do |r|
      r.should_be_samexml_as '<div><a href="http://amrita2.rubyforge.org/">Amrita</a></div>'
    end
  end
  
  specify "inner dynamic element" do
    #text = '<div><a am:src="aaa|Attr[:href=>:url]"><span am:src="inner"/></a></div>'
    text = <<-END
      <<div<
        <<a :aaa | Attr[:href=>:url]<
          <<:inner>>
    END
    t = Amrita2::Template.new(text)
    t.test_with(:aaa => {
                  :url=>"http://amrita2.rubyforge.org/",
                  :inner=>"Amrita"
                }
                ) do |r|
      r.should_be_samexml_as '<div><a href="http://amrita2.rubyforge.org/">Amrita</a></div>'
    end
    
    text = '<div><a am:src="aaa|Attr[:href=>:url]">(<em><span am:src="inner"/></em>)</a></div>'
    t = Amrita2::Template.new(text)
    t.test_with(:aaa => {
                  :url=>"http://amrita2.rubyforge.org/",
                  :inner=>"Amrita"
                }
                ) do |r|
      r.should_be_samexml_as '<div><a href="http://amrita2.rubyforge.org/">(<em>Amrita</em>)</a></div>'
    end
  end
  
  specify "render with struct" do
    t = Amrita2::Template.new('<div><a am:src="aaa|Attr[:href=>:url, :body=>:text]">aaa</a></div>')
    s = Struct.new(:url, :text)
    data = s.new("http://amrita2.rubyforge.org/", "Amrita")
    t.test_with(:aaa => data) do |r|
      r.should_be_samexml_as '<div><a href="http://amrita2.rubyforge.org/">Amrita</a></div>'
    end
  end
  
  specify "static and dynamic attribute" do
    t = Amrita2::Template.new('<div><a class="cls1" am:src="aaa|Attr[:href, :body]">aaa</a></div>')
    t.render_with(:aaa => {
            :href=>"http://amrita2.rubyforge.org/",
            :body=>"Amrita"
        }
    ).should_be_samexml_as '<div><a href="http://amrita2.rubyforge.org/" class="cls1">Amrita</a></div>'
  end
  
  specify "render with binding" do 
    text = '<div><a class="cls1" am:filter="Attr[:body=>true, :href=>:url1]">aaa</a></div>'
    t = Amrita2::Template.new(text, :inline_ruby)
    @url1="http://amrita2.rubyforge.org/"
    @body="Amrita"
    t.test_with(binding) do |r|
      r.should_be_samexml_as '<div><a href="http://amrita2.rubyforge.org/" class="cls1">Amrita</a></div>'
    end
  end    

  specify "render with binding and local variables" do 
    text = '<div><a class="cls1" am:filter="Attr[:body=>true, :href=>:url1]">aaa</a></div>'
    t = Amrita2::Template.new(text, :inline_ruby)
     url1="http://amrita2.rubyforge.org/"
     body="Amrita"
     t.test_with(binding) do |r|
       r.should_be_samexml_as '<div><a href="http://amrita2.rubyforge.org/" class="cls1">Amrita</a></div>'
     end
  end
  
  specify "pickup" do
    #t = Amrita2::Template.new('<div><a am:src="aaa|NVarForAttr[:url]" href="$1"><span am:src="body" /></a></div>')
    t = Amrita2::Template.new <<-END
      <<div<
        <<a href="$1" :aaa|NVarForAttr[:url] <
          <<:body>>
    END
    t.test_with(:aaa => {
                    :url=>"http://amrita2.rubyforge.org/",
                    :body=>"Amrita"
                  }
                  ) do |r|
      r.should_be_samexml_as '<div><a href="http://amrita2.rubyforge.org/">Amrita</a></div>'
    end
  end
  
  specify "pickup with erb" do
    #t = Amrita2::Template.new('<div><a am:src="aaa|NVarForAttr[:url]" href="$1"><%= $_[:body] %></a></div>')
    t = Amrita2::Template.new <<-END
      <<div<
        <<a href="$1" :aaa|NVarForAttr[:url] <
          %= $_[:body]
    END
    t.test_with(:aaa => {
                    :url=>"http://amrita2.rubyforge.org/",
                    :body=>"Amrita"
                  }
                  ) do |r|
      r.should_be_samexml_as '<div><a href="http://amrita2.rubyforge.org/">Amrita</a></div>'
    end
  end
  
  specify "Nvar with $n" do
    t = Amrita2::Template.new('<a am:filter="NVar[:aaa]" href="$1" class="$$1">$$2</a>')
    t.test_with(:aaa => 'aaa') do |r|
      r.should_be_samexml_as "<a href='aaa' class='$1'>$2</a>"
    end
  end
end

