require 'amrita2'

context "Hash Delegator" do
  include Amrita2

  specify "delegate to Hash" do
    h = { :one => 1 }
    c = Amrita2::HashDelegator.new(h) do
      { 
        :two=>2,
      }
    end
    c.one.should == 1
    c.two.should == 2
    c[:one].should == 1
    c[:two].should == 2
    c.three.should be_nil
    c[:three].should be_nil
  end
  
  specify "delegate to Struct" do
    s = Struct.new(:one).new(1)
    c = Amrita2::HashDelegator.new(s) do
      { 
        :two=>2,
      }
    end
    c.one.should == 1
    c.two.should == 2
    c[:one].should == 1
    c[:two].should == 2
    proc { c.three }.should raise_error
    c[:three].should be_nil
  end
  
  specify "double delegate to Struct" do
    s = Struct.new(:one).new(1)
    c1 = Amrita2::HashDelegator.new(s) do
      { 
        :two=>2,
      }
    end
    c2 = Amrita2::HashDelegator.new(c1) do
      { 
        :three=>3,
      }
    end
    c2.one.should == 1
    c2.two.should == 2
    c2[:one].should == 1
    c2[:two].should == 2
    c2.three.should == 3
    c2[:three].should == 3
    proc { c2.four }.should raise_error
    c2[:four].should be_nil
  end
end
