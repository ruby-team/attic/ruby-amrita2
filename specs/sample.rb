require 'amrita2'
require 'amrita2/testsupport'

module SampleTestSupport
  def get_result(dir, fname)
    Dir::chdir("sample/#{dir}") do
      IO.popen("ruby -rubygems -I../../lib #{fname}") do |f|
        f.read
      end
    end
  end

  def get_expected(dir, fname)
    File::open("sample/#{dir}/#{fname}") do |f|
      f.read.split(/__END__/m).last
    end
  end

  def compare(dir, fname)
    result = get_result(dir, fname)
    expected = get_expected(dir, fname)
    result.should_be_samexml_as expected
  end
end


context "hello" do
  include SampleTestSupport
  
  specify "hello.rb" do
    compare('hello', 'hello.rb')
  end
end
