require 'rexml/document'
require 'amrita2'
require 'amrita2/testsupport'
require 'amrita2/template'
require 'amrita2/gettext'
require 'gettext/utils'
require 'fileutils'

include Amrita2
include Amrita2::Filters
include Amrita2::GetTextBridge

require File::dirname(__FILE__) + '/gettext_util'

$KCODE='u'

context "gettext erb element" do
  include Amrita2::Runtime
  include Amrita2GetTextTestSupport

  specify "simple erb element" do
    domain = "erb1"
    temp = "/tmp/amrita_gettext_test.a2html"
    Locale.set_current("ja", "JP", "utf8")
    b = binding
    test_gettext(
                 domain,
                 temp,
                 "<span><![CDATA[ <%=_('test message printed in ERB')%> ]]></span>", 
                 [
                   %r[#: #{temp}:-],
                   %r[msgid "test message printed in ERB"],
                 ],
                 b, 
                 "ERBで出力したテストメッセージ"
                 ) do |l|
      l.sub!(%r[msgid "test message printed in ERB"\nmsgstr ""]m, 
              %[msgid "test message printed in ERB"\nmsgstr "ERBで出力したテストメッセージ"])
      l
    end
  end
end
