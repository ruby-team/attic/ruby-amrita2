
require 'rexml/document'
require 'amrita2'
#require 'amrita2/macro'
require 'amrita2/testsupport'

context "Minimum Template" do
  setup do
    @t = Amrita2::Template.new('<span am:src="aaa" />')
  end
  
  specify "delete span without attributes" do
    result = "Amrita2"
    #@t.render_with(:aaa=>'Amrita2').should_be_samexml_as(result)
    @t.render_with(:aaa=>'Amrita2').should_be_samexml_as 'Amrita2'
  end
end

context "single tag" do
  setup do
    @t = Amrita2::Template.new('<p>aa<br />cc </p>')
  end
  
  specify "delete span without attributes" do
    result = "Amrita2"
    #@t.render_with(:aaa=>'Amrita2').should_be_samexml_as(result)
    @t.render_with(binding).should_be_samexml_as '<p>aa<br />cc</p>'
  end
end

context "Compiletime Binding" do
  setup do
    @t = Amrita2::Template.new('<<:aaa|MyFilter|Repeat[3]>>')
  end

  module CompiletimeBindingTest
    include Amrita2::CompileTimeContext
    class MyFilter < Amrita2::Filters::Base
      def value_filter_code(de, cg, value)
        cg.code("$_ = $_ + '_myfilter'")
        super
      end
    end

    Binding = binding
  end

  specify "not setting binding " do
    proc { @t.render_with(:aaa=>'test')}.should raise_error
  end
  
  specify "setting binding " do
    @t.compiletime_binding = CompiletimeBindingTest::Binding
    @t.test_with(:aaa=>'test') do |r|
      r.should_be_samexml_as("test_myfilter" * 3)
    end
  end
end

context "change command attributes" do
  specify "test:src" do
    t = Amrita2::Template.new('<div><span test:src="aaa" /></div>')
    t.amrita_prefix = "test:"
    expected = "<div>Amrita2</div>"
    t.test_with(:aaa=>'Amrita2') do |result|
      result.should_be_samexml_as(expected)
    end
  end
  
  specify "test:src with option for constructor" do
    t = Amrita2::Template.new('<div><span test:src="aaa" /></div>', :amrita_prefix => 'test:')
    expected = "<div>Amrita2</div>"
    t.test_with(:aaa=>'Amrita2') do |result|
      result.should_be_samexml_as(expected)
    end
  end
  
  specify "use amrita_id and filter" do
    t = Amrita2::Template.new(%[<div><span amrita_id="aaa" filter="Default['Amrita2']" /></div>])
    t.amrita_src = "amrita_id"
    t.amrita_filter = "filter"
    t.test_with(:aaa=>nil) do |result|
      expected = "<div>Amrita2</div>"
      result.should_be_samexml_as(expected)
    end
  end
end

context "xhtml" do
  specify "doc type" do
    t = Amrita2::Template.new <<END, :process_xhtml=>true
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
 ! comment
-->
<<html xmlns="http://www.w3.org/1999/xhtml" lang="en"<
  <<head<
    <<title:title>>
  <<body:body>>
END
  
    t.test_with(:title=>'Amrita2', :body=>'Amrita2 can process XHTML') do |result|
      result.should match(/<\!DOCTYPE/) 
      result.should_be_samexml_as <<-END
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
 ! comment
-->
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<title>Amrita2</title>
</head>
<body>Amrita2 can process XHTML</body>
</html>
END
    end
  end
  
  specify "all" do
    t = Amrita2::Template.new <<END
<%(BeforeCompile) self.opt[:process_xhtml]=true %>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<title am:src="title" />
</head>
<body am:src="body"/>
</html>
END
  
    t.test_with(:title=>'Amrita2', :body=>'Amrita2 can process XHTML') do |result|
      result.should match(/<\?xml/)
      result.should match(/<\!DOCTYPE/) 
      result.should_be_samexml_as <<-END
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<title>Amrita2</title>
</head>
<body>Amrita2 can process XHTML</body>
</html>
END
    end
  end
end

context "Comment" do
  setup do
    @t = Amrita2::Template.new('<!--aaa--><span am:src="aaa" /><!--bbb-->')
  end
  
  specify "delete span without attributes" do
    result = "Amrita2"
    #@t.render_with(:aaa=>'Amrita2').should_be_samexml_as(result)
    @t.render_with(:aaa=>'Amrita2').should_be_samexml_as '<!--aaa-->Amrita2<!--bbb-->'
  end
end

context "nbsp;" do
  include Amrita2::Runtime
  specify "keep &nbsp;" do
    t = Amrita2::Template.new('<a>A&nbsp;B</a>')
    t.render_with(binding).should == '<a>A&nbsp;B</a>'
  end
  
  specify "keep &nbsp; multi line" do
    t = Amrita2::Template.new <<-END
      <<div<
        A
        &nbsp;|&nbsp;
        B
    END
    
    t.render_with(binding).should == <<-END.strip
<div>        A
        &nbsp;|&nbsp;
        B
</div>
    END
  end
  
  specify "keep &nbsp; in data" do
    t = Amrita2::Template.new('<<:xyz>>')
    t.render_with(:xyz=>'A&nbsp;B').should == 'A&nbsp;B'
  end
  
end
