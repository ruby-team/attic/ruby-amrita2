
require 'rexml/document'
require 'amrita2'
require 'amrita2/testsupport'


context "Sanitize" do
include Amrita2
include Amrita2::Filters
  
  setup do
    @t = Amrita2::Template.new('<span><span am:src="aaa" /></span>')
  end
  
  specify "no sanitize" do
    @t.render_with(:aaa=>'Amrita2').should_be_samexml_as("Amrita2")
  end

  specify "do sanitize" do
    @t.render_with(:aaa=>'&<>').should_be_samexml_as("&amp;&lt;&gt;")
    @t.render_with(:aaa=>'<strong>Amrita2</strong>').should_be_samexml_as("&lt;strong&gt;Amrita2&lt;/strong&gt;")
  end

  specify "sanitized string" do
    @t.render_with(:aaa=>Amrita2::SanitizedString['<strong>Amrita2</strong>']).should_be_samexml_as('<strong>Amrita2</strong>')
  end

  specify "NoSanitize filter" do
    t = Amrita2::Template.new('<span><span am:src="aaa|NoSanitize" /></span>')
    t.render_with(:aaa=>'<strong>Amrita2</strong>').should_be_samexml_as('<strong>Amrita2</strong>')
  end
end

context "Sanitize attributes" do
include Amrita2
  setup do
    @t = Amrita2::Template.new('<span><a am:src="aaa|Attr[:href, :title, :body]" /></span>')
  end
  
  specify "no sanitize" do
    data = {
      :aaa=> {
        :href=>'http://www.ruby-lang.org/',
        :body=>'Ruby'
      }
    }
    expected = "<a href='http://www.ruby-lang.org/'>Ruby</a>"
    @t.render_with(data).should_be_samexml_as(expected)
  end

  specify "do sanitize" do
    data = {
      :aaa=> {
        :href=>'http://www.ruby-lang.org/',
        :title=>"Ruby's home page",
        :body=>'<Ruby>'
      }
    }
    expected = %[<a href="http://www.ruby-lang.org/" title="Ruby's home page">&lt;Ruby&gt;</a>]
    @t.test_with(data) do |result|
      result.should_be_samexml_as(expected)
    end
  end
  
  specify "do sanitize double quotes" do
    data = {
      :aaa=> {
        :href=>'http://www.ruby-lang.org/',
        :title=>"\"Ruby's home page\"",
        :body=>'<Ruby>'
      }
    }
    expected = %[<a href="http://www.ruby-lang.org/" title="&quot;Ruby's home page&quot;">&lt;Ruby&gt;</a>]
    @t.test_with(data) do |result|
      result.should_be_samexml_as(expected)
    end
  end

  specify "stop sanitize" do
    data = {
      :aaa=> {
        :href=>'http://www.ruby-lang.org/',
        :title=>"Ruby's home page",
        :body=>Amrita2::SanitizedString['<strong>Ruby</strong>']
      }
    }
    expected = %[<a href='http://www.ruby-lang.org/' title="Ruby's home page"><strong>Ruby</strong></a>]
    @t.test_with(data) do |result|
      result.should_be_samexml_as(expected)
    end
  end
end

context "Sanitize attributes with NVar" do
include Amrita2
  specify "do sanitize " do
    t = Amrita2::Template.new('<span><a am:src="aaa|NVar[:url, :body]" href="$1">$2</a></span>')
    t.test_with(:aaa =>{:url=>'a"b"c', :body=>'efg'}) do |result|
      result.should_be_samexml_as('<a href="a&quot;b&quot;c">efg</a>')
    end
    
    t = Amrita2::Template.new('<span><a am:src="aaa|NVarForAttr[:url]" href="$1">efg</a></span>')
    t.test_with(:aaa =>{:url=>'a"b"c', :body=>'efg'}) do |result|
      result.should_be_samexml_as('<a href="a&quot;b&quot;c">efg</a>')
    end
  end
end

context "Skip Sanitize to result of Template" do
include Amrita2
  
  setup do
    @t1 = Amrita2::Template.new('<p><span am:src="aaa" /></p>')
    @t2 = Amrita2::Template.new('<body><span am:src="aaa" /></body>')
  end
  
  specify "no sanitize " do
    a = @t1.render_with(:aaa=>'Amrita2')
    a.should_be_samexml_as("<p>Amrita2</p>")
    @t2.test_with(:aaa=>a) do |result|
      result.should_be_samexml_as("<body><p>Amrita2</p></body>")
    end
  end
end
