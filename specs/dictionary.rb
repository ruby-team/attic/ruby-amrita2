
require 'rexml/document'
require 'amrita2'
require 'amrita2/testsupport'

include Amrita2

context "Dictionary Data" do
  setup do
    @t = Amrita2::Template.new('<div><span am:src="aaa" /></div>')
  end
  
  specify "Struct" do
    s = Struct.new(:aaa, :bbb)
    data = s.new(123, 456)
    result = "<div>123</div>"
    @t.render_with(data).should_be_samexml_as(result)
  end

  class MyData
    include Amrita2::DictionaryData

    def aaa
      "MyData"
    end
  end

  specify "Struct" do
    data = MyData.new
    result = "<div>MyData</div>"
    @t.render_with(data).should_be_samexml_as(result)
  end

  specify "Array of Struct" do
    t = Amrita2::Template.new('<div><span am:src="xxx"><span am:src="aaa"/></span></div>')
    data = {
      :xxx=> [MyData.new, MyData.new]
    }
    t.test_with(data) do |result|
      result.should_be_samexml_as "<div>MyDataMyData</div>"
    end
  end

  specify "Binding" do
    @aaa = 'Binding'
    @t.test_with(binding) do |result|
      result.should_be_samexml_as "<div>Binding</div>"
    end
  end
  
  specify "Binding for local variables" do
    aaa = 'Binding'
    @t.test_with(binding) do |result|
      result.should_be_samexml_as "<div>Binding</div>"
    end
  end

  specify "mix" do
    t = Amrita2::Template.new('<div><span am:src="xxx"><span am:src="yyy"><span am:src="aaa"/></span></span></div>')
    @xxx = {
      :yyy=> MyData.new
    }
    t.test_with(binding) do |result|
      result.should_be_samexml_as "<div>MyData</div>"
    end
  end
end

