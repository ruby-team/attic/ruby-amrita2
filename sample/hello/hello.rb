require "amrita2/template"
include Amrita2

tmpl = Template.new <<-END
<<html<
  <<body<
    <<h1 :title>>
    <<p :body<
      <<:template>> is a html template libraly for <<:lang>>
END

#tmpl.set_trace(STDOUT)
puts tmpl.render_with(:title=>"hello world", :body=>{ :template=>"Amrita2", :lang=>"Ruby" })

__END__

<html>
   <body>
      <h1>hello world</h1>
      <p>Amrita2 is a html template libraly for Ruby</p>
   </body>
</html>
